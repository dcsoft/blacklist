# Blacklist.dll

A simple C library (written in VC++) implementing a custom hash table.  Urls and their 'categories' (a short int identifying the type of url) are quickly hashed and stored in one contiguous block of shared memory for very fast lookup and low memory usage.

MD-4 hashing was selected instead of the better known MD-5 because it is known to be faster.  It is less secure but because this hashing is not used for security <i>i.e. hashing of passwords</i>, it is a good tradeoff.


## Commerical Impact
This DLL was used in [Salfeld Parental Control](https://salfeld.com/en/software/parental-control/) which monitors the urls the user's browser is navigating to, and stops them if they are in the 'blacklist' hash table. 

This DLL replaced a Delphi generic hash table and improved performance from seconds per lookup to near instantaneous, thus making the product commercially viable.


## Thanks
 
Much thanks to Dierk Salfeld for allowing this commercial DLL to be presented here for demonstration purposes.