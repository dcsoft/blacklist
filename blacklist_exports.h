#ifdef BLACKLIST_EXPORTS
	#define BLACKLIST_API	__declspec(dllexport)
#else
	#define BLACKLIST_API	__declspec(dllimport)
#endif


	// Return value of LoadBlacklist()
enum
{
	CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS = 0,
	CREATE_BLACKLIST_SHARED_MEMORY_EXISTED = 1,
	CREATE_BLACKLIST_SHARED_MEMORY_SID_FAILED = 2,
	CREATE_BLACKLIST_SHARED_MEMORY_ACL_FAILED = 3,
	CREATE_BLACKLIST_SHARED_MEMORY_ALLOC_SD_FAILED = 4,
	CREATE_BLACKLIST_SHARED_MEMORY_INIT_SD_FAILED = 5,
	CREATE_BLACKLIST_SHARED_MEMORY_ADD_TO_DACL_FAILED = 6,
	CREATE_BLACKLIST_SHARED_MEMORY_FILE_MAPPING_FAILED = 7,
};

enum
{
	INIT_BLACKLIST_SUCCESS = 0,
};

enum
{
	ADD_BLACKLIST_URL_SUCCESS = 0,
	ADD_BLACKLIST_URL_SHARED_MEMORY_NOT_INIT = 201,
	ADD_BLACKLIST_TOO_MANY_URLS = 202,
};

enum
{
	FINALIZE_BLACKLIST_SUCCESS = 0,
};



enum
{
	LOAD_BLACKLIST_SUCCESS = 0,
	LOAD_BLACKLIST_ALREADY_LOADED = 101,
	LOAD_BLACKLIST_FILE_NOT_FOUND = 102,
	LOAD_BLACKLIST_LINE_NOT_READ = 103,
};


extern "C"
{
	// Optional, will be called from the other API's as necessary
	int BLACKLIST_API CreateBlacklistSharedMemory();

	// Normal usage:  Init, Add each blacklist url (and it's category), then Finalize
	int BLACKLIST_API InitBlacklist();
	int BLACKLIST_API AddBlacklistUrl(const char *url, int category);
	int BLACKLIST_API FinalizeBlacklist(int sort);		// sort = 0 ==> linear search, sort = 1 ==> binary search

	// Load urls from file 'blacklist-cat-tab.txt' in the folder containing the DLL
	int BLACKLIST_API LoadBlacklist(int sort);

	// Search for an url and return it's category or 0 if not found
	int BLACKLIST_API GetBlacklistCategory(const char *url);
}
