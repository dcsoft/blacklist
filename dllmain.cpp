// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include "blacklist_exports.h"

#include "md4.h"


#define MAX_URLS		(4000000)
#define NUM_HASH_BYTES	(16)
#define NUM_CATEGORY_BYTES (2)
#define NUM_ELEMENT_BYTES (NUM_HASH_BYTES + NUM_CATEGORY_BYTES)




//////////////////////////////////////////////////////////////////////////
// Global variables - process specific

struct MetaData
{
	int m_nNumHashes;
	int m_nSort;
};

static HINSTANCE g_hModule = NULL;
static MetaData *g_pMetaData = NULL;
static BYTE *g_pUrlHashes = NULL;



//////////////////////////////////////////////////////////////////////////
// MetaData Functions

inline int NumHashes()
{
	return g_pMetaData->m_nNumHashes;
}

inline void NumHashes(int n)
{
	g_pMetaData->m_nNumHashes = n;
}


inline int Sort()
{
	return g_pMetaData->m_nSort;
}

inline void Sort(int n)
{
	g_pMetaData->m_nSort = n;
}



// Source:  http://www.opensource.apple.com/source/freeradius/freeradius-11/freeradius/src/lib/md4.c
static void md4_calc(BYTE *output, BYTE *input, unsigned int inlen)
{
	MD4_CTX	context;

	MD4_Init(&context);
	MD4_Update(&context, input, inlen);
	MD4_Final(output, &context);
}



//////////////////////////////////////////////////////////////////////////
// Add urls to hash table

static int CompareHashFunc(const void *p1, const void *p2)
{
	return memcmp(p1, p2, NUM_HASH_BYTES);
}

static void SortBlacklist()
{
	qsort(g_pUrlHashes, NumHashes(), NUM_ELEMENT_BYTES, CompareHashFunc);
}


//////////////////////////////////////////////////////////////////////////
// Exported API

int CreateBlacklistSharedMemory()
{
	// Initialize an EXPLICIT_ACCESS structure for an ACE.
	// The ACE will allow Everyone full access to the
	// file mapping object.
	//

	DWORD	dwRes;
	PSID	pEveryoneSID = NULL;
	PSECURITY_DESCRIPTOR pSD = NULL;
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
	SECURITY_ATTRIBUTES sa;
	HANDLE m_hMapObject;
	int ret = CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS;

	// Abort if there is a view onto the shared memory already
	if (g_pUrlHashes)
		return ret;

	// Create a well-known SID for the Everyone group.
	if(! AllocateAndInitializeSid( &SIDAuthWorld, 1,
		SECURITY_WORLD_RID,
		0, 0, 0, 0, 0, 0, 0,
		&pEveryoneSID) )
	{
		return CREATE_BLACKLIST_SHARED_MEMORY_SID_FAILED;
	}

	EXPLICIT_ACCESS ea;
	ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));
	ea.grfAccessPermissions = STANDARD_RIGHTS_ALL | SPECIFIC_RIGHTS_ALL;
	ea.grfAccessMode = SET_ACCESS;
	ea.grfInheritance= NO_INHERITANCE;
	ea.Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea.Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea.Trustee.ptstrName  = (LPTSTR) pEveryoneSID;

	// Create a new ACL that contains the new ACE.

	PACL pACL = NULL;
	dwRes = SetEntriesInAcl(1, &ea, NULL, &pACL);
	if (ERROR_SUCCESS != dwRes)
	{
		ret = CREATE_BLACKLIST_SHARED_MEMORY_ACL_FAILED;
		goto Cleanup;
	}

	// Initialize a security descriptor.  

	pSD = (PSECURITY_DESCRIPTOR) LocalAlloc(LPTR, SECURITY_DESCRIPTOR_MIN_LENGTH); 
	if (pSD == NULL)
	{ 
		ret = CREATE_BLACKLIST_SHARED_MEMORY_ALLOC_SD_FAILED;
		goto Cleanup; 
	} 

	if (!InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION))
	{  
		ret = CREATE_BLACKLIST_SHARED_MEMORY_INIT_SD_FAILED;
		goto Cleanup; 
	} 

	// Add the ACL to the security descriptor. 

	if (!SetSecurityDescriptorDacl(pSD, 
		TRUE,     // fDaclPresent flag   
		pACL, 
		FALSE))   // not a default DACL 
	{
		ret = CREATE_BLACKLIST_SHARED_MEMORY_ADD_TO_DACL_FAILED;
		goto Cleanup; 
	} 

	// Initialize a security attributes structure.

	sa.nLength = sizeof (SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = pSD;
	sa.bInheritHandle = FALSE;

	// Use SEH!!
	// Create a named file mapping object.
	int nSizeLow = sizeof(MetaData) + (MAX_URLS * NUM_ELEMENT_BYTES);
	m_hMapObject = CreateFileMapping( 
		INVALID_HANDLE_VALUE, // use paging file
		&sa,				  // Everyone needs access to security attribute
		PAGE_READWRITE,       // read/write access
		0,                    // size: high 32-bits
		nSizeLow,  // size: low 32-bits
		"Global\\SALFELD-{820F4604-3849-468a-B67E-99432B1E42EF}");   // name of map object

	if (!m_hMapObject)
	{
		ret = CREATE_BLACKLIST_SHARED_MEMORY_FILE_MAPPING_FAILED;
		goto Cleanup;
	}

	// File mapping is created
	// Was file mapping already there?
	bool existed = (GetLastError() == ERROR_ALREADY_EXISTS);
	
Cleanup:
	// Clean up, and return if error so far (file mapping not created)
	if (pEveryoneSID)
		FreeSid(pEveryoneSID);
	if (pACL)
		LocalFree(pACL);
	if (pSD)
		LocalFree(pSD);

	if (ret != CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS)
		return ret;


	// Get a pointer to the file-mapped shared memory.
	g_pMetaData = (MetaData *) MapViewOfFile( 
		m_hMapObject,     // object to map view of
		FILE_MAP_WRITE, // read/write access
		0,              // high offset:  map from
		0,              // low offset:   beginning
		0);             // default: map entire file

	g_pUrlHashes = ((BYTE *) g_pMetaData) + sizeof(MetaData);	// hash array located directly after metadata


	// Init file mapping if freshly created
	if (!existed)
	{
		NumHashes(0);
	}

	return (existed) ? CREATE_BLACKLIST_SHARED_MEMORY_EXISTED : CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS;
}


int BLACKLIST_API InitBlacklist()
{
	int ret = CreateBlacklistSharedMemory();
	if (ret != CREATE_BLACKLIST_SHARED_MEMORY_EXISTED && ret != CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS)
		return ret;

	// Init meta data
	NumHashes(0);
	Sort(0);

	return INIT_BLACKLIST_SUCCESS;
}


int AddBlacklistUrl(const char *url, int category)
{
	if (!g_pUrlHashes)
		return ADD_BLACKLIST_URL_SHARED_MEMORY_NOT_INIT;

	int i = NumHashes();
	if (i >= MAX_URLS)
		return ADD_BLACKLIST_TOO_MANY_URLS;

	// Compute hash of url
	BYTE hash[NUM_HASH_BYTES];
	md4_calc(hash, (BYTE *)url, lstrlen(url));

	// Save hash
	BYTE *p = g_pUrlHashes + (NUM_ELEMENT_BYTES * i);
	CopyMemory(p, hash, NUM_HASH_BYTES);

	// Save category
	short *pCategory = (short *) (p + NUM_HASH_BYTES);
	*pCategory = category;

	// Inc num saved hashes
	NumHashes(i + 1);

	return ADD_BLACKLIST_URL_SUCCESS;
}


int FinalizeBlacklist(int sort)
{
	Sort(sort);		// save sort

	if (sort)
		SortBlacklist();

	return FINALIZE_BLACKLIST_SUCCESS;
}



int LoadBlacklist(int sort)
{
	int ret = InitBlacklist();
	if (ret != INIT_BLACKLIST_SUCCESS)
		return ret;

	if (NumHashes() > 0)
		return LOAD_BLACKLIST_ALREADY_LOADED;

	// Load blacklist from file
	char szBlacklistPath[MAX_PATH];
	GetModuleFileName(g_hModule, szBlacklistPath, _countof(szBlacklistPath));
	lstrcpy(PathFindFileName(szBlacklistPath), "blacklist-cat-tab.txt");

	FILE *f = NULL;
	fopen_s(&f, szBlacklistPath, "r");
	if (!f)
	{
		return LOAD_BLACKLIST_FILE_NOT_FOUND;
	}

	char szUrl[4096];
	int category;
	int nMalformed = 0;
	while (!feof(f))
	{
		// Source:  How To sscanf() Example Using a Comma (,) as Delimiter
		// http://support.microsoft.com/kb/38335
		ret = fscanf_s(f, "%[^\t]\t%d\n", szUrl, _countof(szUrl), &category);
		if (ret == 0)
			nMalformed++;

		ret = AddBlacklistUrl(szUrl, category);
		if (ret != ADD_BLACKLIST_URL_SUCCESS)
		{
			fclose(f);
			return ret;
		}
	}

	fclose(f);

	ret = FinalizeBlacklist(sort);
	if (ret != FINALIZE_BLACKLIST_SUCCESS)
		return ret;

	return LOAD_BLACKLIST_SUCCESS;
}


int GetBlacklistCategory(const char *url)
{
	int ret = CreateBlacklistSharedMemory();
	if (ret != CREATE_BLACKLIST_SHARED_MEMORY_EXISTED && ret != CREATE_BLACKLIST_SHARED_MEMORY_SUCCESS)
		return 0;

	// Compute url hash
	BYTE hash[NUM_HASH_BYTES];
	md4_calc(hash, (BYTE *)url, lstrlen(url));

	// Lookup url hash
	int n = NumHashes();

	if ( Sort() )
	{
		// bsearch - If the array is not in ascending sort order or contains duplicate records with identical keys, the result is unpredictable.!!!!
		// Source:  http://msdn.microsoft.com/en-us/library/w0k41tbs.aspx
		byte *pFoundHash = (byte *) bsearch(hash, g_pUrlHashes, n, NUM_ELEMENT_BYTES, CompareHashFunc);
		if (!pFoundHash)
			return 0;

		short *pCategory = (short *) (pFoundHash + NUM_HASH_BYTES);
		return *pCategory;
	}
	else
	{
		for (int i = 0; i < n; i++)
		{
			BYTE *pHash = g_pUrlHashes + (NUM_ELEMENT_BYTES * i);
			if (memcmp(pHash, hash, NUM_HASH_BYTES) == 0)
			{
				short *pCategory = (short *) (pHash + NUM_HASH_BYTES);
				return *pCategory;
			}
		}
	}

	return 0;	// hash not found
}



//////////////////////////////////////////////////////////////////////////
// DLL Entrypoint

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			g_hModule = hModule;
			break;

		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}

